
<?php
// INSERCION DE LA CATEGORIA EN LA BBDD 
if(isset($_POST['insertarCategoria'])){
	$nombre=$_POST['nombre'];
	$sql="INSERT INTO categorias(nombreCategoria)VALUES('$nombre')";

	//Ejecuto la consulta. Si no es igual muestro error si la consulta es correcta la incluyo y listo
	if(!$consulta=$conexion->query($sql)){
	?>
	<div class="alert alert-danger">
		<strong>ERROR!!</strong>
		No se ha podido realizar
	</div>
	<?php
	}
}
?>

<h3 class="text-right">
	Categorías de Noticias
</h3>

<ul class="nav nav-pills nav-stacked">
<?php 
//Poner active en el enlace a TODAS si no le paso idCategoria
if(isset($_GET['idCategoria'])){
	$activo='';
	$idCategoria=$_GET['idCategoria'];
}else{
	$activo='active';
	$idCategoria=0;
}
?>

	<li class="text-right  <?php echo $activo; ?>">
		<a href="index.php?p=<?php echo $p ?>">
			Ver todas las Noticias
		</a>		
	</li>

	<?php 
	//LISTADO DE CATEGORIAS

	//Establezco una consulta segun su id de noticia
	$sql="SELECT * FROM categorias ORDER BY nombreCategoria ASC";

	// Ejecuto la consulta
	$consulta=$conexion->query($sql);

	// Extraigo y proceso los datos de dicha consulta

	while($registro=$consulta->fetch_array()){
		if($idCategoria==$registro['idCategoria']){
			$activo='active';
		}else{
			$activo='';
		}
	?>
		<li class="text-right <?php echo $activo; ?>">
			<a href="index.php?p=<?php echo $p ?>&idCategoria=<?php echo $registro['idCategoria']; ?>">
				<?php echo $registro['nombreCategoria']; ?>
			</a>		
		</li>

	<?php
	}
	?>
</ul>

<hr>

<?php 
// FORMULARIO PARA INSERTAR LA CATEGORIA
?>

<!-- <h4 class="text-right">Nueva Categoría</h4>
<form action="index.php?p=noticias.php" method="post" class="text-right">
	<div class="form-group">
		<label for="titulo">Nombre de la categoría:</label>
		<input type="text" class="form-control" name="nombre" id="nombre">
	</div> 

	<button type="sumbit" name="insertarCategoria" class="btn btn-dfault">
		Insertar Categoría
	</button>
</form> -->



