

<?php 
  session_start();
  require('includes/conexion.php');

//Recojo la pagina que quiero mostrar
  if(isset($_GET['p'])){
    $p=$_GET['p'];
  }else{
    $p='inicio.php'; //Pagina INICIAL
  }
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Noticias - José Ignacio Lahoz</title>
 
    <!-- CSS de Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
 
  </head>
  <body>
    
    <section class="container">
      <!-- ENCABEZADO -->
   
      <?php 
        include('includes/encabezado.php');
      ?>
      
       <?php 
        include('includes/login.php');
      ?>

      <!-- MENU DE NAVEGACION -->
    
      <?php 
        include('includes/menu.php');
      ?>

     <!--  SECCIÓN CENTRAL
        COLUMNA IZDA 8 HUECOS EN DISPOSITIVOS MEDIANOS ADELANTE
        COLUMNA DCHEA 4 HUECOS EN DISPOSITIVOS MEDIANOS ADELANTE -->
    
      <main class="row">
      
        <section class="col-md-8">
            <?php include('paginas/'.$p); ?>
        </section>

        <nav class="col-md-4">
            <?php 
              include('includes/categorias.php');
            ?> 
        </nav>

      </main>

      <!-- PIE -->
   
      <?php 
        include('includes/pie.php');
      ?> 


    </section>  


    <!-- Cargar Librería jQuery requerida por los plugins de JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>
 
    <!-- Todos los plugins JavaScript de Bootstrap (también puedes
         incluir archivos JavaScript individuales de los únicos
         plugins que utilices) -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>

<?php 
//Desconectar de la base de datos
$conexion->close();

?>