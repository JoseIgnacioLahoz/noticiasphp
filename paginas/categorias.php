
<h3>
	Listado de Categorías
	-
	<?php 
		if($_SESSION['conectado']){
	?>
	<small>
		<a href="index.php?p=insertarCategoria.php">Insertar categoría</a>
	</small>
	<?php } ?>
</h3>
<table class="table table-hover table-striped">

	<?php 
	//LISTADO DE CATEGORIAS

	//Establezco una consulta segun su id de categoria
	$sql="SELECT * FROM categorias ORDER BY nombreCategoria ASC";

	// Ejecuto la consulta
	$consulta=$conexion->query($sql);

	// Extraigo y proceso los datos de dicha consulta y los muestro

	while($registro=$consulta->fetch_array()){
	?>
		<tr>
			<td>
				<?php echo $registro['nombreCategoria']; ?>
			</td>
			<?php 
				if($_SESSION['conectado']){
			?>
			<td class="text-right">
				<a href="index.php?p=modificarCategoria.php&idCategoria=<?php echo $registro['idCategoria'];?>">Modificar</a>
				-
				<a href="index.php?p=borrarCategoria.php&idCategoria=<?php echo $registro['idCategoria'];?>">Borrar</a>
			</td>
			<?php } ?>	
		</tr>

	<?php
	}
	?>
</table>

<hr>