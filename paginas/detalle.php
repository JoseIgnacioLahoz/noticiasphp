<h3>
	Detalle de Noticia
</h3>

<?php 
// Recojo el idNoticia que quiero mostrar
$idNoticia=$_GET['idNoticia'];

//Establezco una consulta segun su id de noticia
$sql="SELECT * FROM noticias  INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria WHERE idNoticia=$idNoticia";

// Ejecuto la consulta
$consulta=$conexion->query($sql);

// Extraigo los datos de dicha consulta
$registro=$consulta->fetch_array();
?>

<article>
	<header>
		<h4>
			<strong><?php echo $registro['tituloNoticia']; ?></strong>
			<a href="index.php?p=noticias.php"> - Volver</a>
		</h4>
		<small>
			<?php echo $registro['nombreCategoria']; ?>
		</small>
	</header>
	<section>
		<img src="imagenes/<?php echo $registro['imagenNoticia']; ?>" class="img-responsive img-rounded">
		<br>
		<?php echo $registro['textoNoticia']; ?>
	</section>
	<footer class="text-right" style="clear: both;">
		<?php echo $registro['fechaNoticia']; ?>
	</footer>
</article>