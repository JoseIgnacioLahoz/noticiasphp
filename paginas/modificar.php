<h3>
	Modificar una noticia
	-
	<small>
		<a href="index.php?p=noticias.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if (isset($_POST['enviar'])){
	//Modifico la noticia
	 
	//Recojo los datos que quiero insertar
	$titulo=$_POST['titulo'];
	$texto=$_POST['texto'];
	$nombreImagen=$_FILES['imagen']['name'];
	$id=$_POST['idNoticia'];
	$categoria=$_POST['categoria'];

	
	if(is_uploaded_file($_FILES['imagen']['tmp_name'])){
		//Muevo la imagen a la carpeta
		move_uploaded_file($_FILES['imagen']['tmp_name'], 'imagenes/'.$nombreImagen);

		//Establezco la consulta
		$sql="UPDATE noticias SET tituloNoticia='$titulo', textoNoticia='$texto', imagenNoticia='$nombreImagen', idCategoria='$categoria' WHERE idNoticia=$id";
	}else{
		//Establezco la consulta sin la imagen
		$sql="UPDATE noticias SET tituloNoticia='$titulo', textoNoticia='$texto', idCategoria='$categoria' WHERE idNoticia=$id";
	}

	//Ejecuto la consulta y/o Muestro el mensaje
	if($consulta=$conexion->query($sql)){
		header('Refresh: 2; url=index.php?p=noticias.php');
		?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Modificación realizada con éxito
			<img src="imagenes/cargando.gif" width="50">
		</div>
		<?php	
	}else{
		?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}
}else{
	//Muestro el formulario de modificación
	//Necesito el id de la noticia a modificar para poder actuar sobre ella
	$idNoticia=$_GET['idNoticia'];
	$sql="SELECT * FROM noticias WHERE idNoticia=$idNoticia";
	$consulta=$conexion->query($sql);
	$registro=$consulta->fetch_array();
?>
<form action="index.php?p=modificar.php" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="titulo">Titulo de la noticia:</label>
		<input type="text" class="form-control" name="titulo" id="titulo" value="<?php echo $registro['tituloNoticia']; ?>">
	</div> 

	<div class="form-group">
		<label for="texto">Texto de la noticia:</label>
		<textarea class="form-control"  name="texto" id="texto"><?php echo $registro['textoNoticia']; ?></textarea>		
	</div>

	<div class="form-group">
		<label for="imagen">Imagen de la noticia:</label>
		<br>
		<img src="imagenes/<?php echo $registro['imagenNoticia']; ?>" width="100">
		<br>
		<input type="file" class="form-control"  name="imagen" id="imagen">
	</div>

	<div class="form-group">
		<label for="categoria">Categoría de la noticia:</label>
		<select name="categoria" id="categoria" class="form-control">
			<?php 
			$sqlCat="SELECT * FROM categorias ORDER BY nombreCategoria ASC";
			$consultaCat=$conexion->query($sqlCat);
			while($registroCat=$consultaCat->fetch_array()){
				if($registro['idCategoria']==$registroCat['idCategoria']){
					$seleccionada='selected';
				}else{
					$seleccionada='';
				}
			?>
				<option value="<?php echo$registroCat['idCategoria']; ?>" <?php echo $seleccionada; ?>>
					<?php echo$registroCat['nombreCategoria']; ?>
				</option>
				<?php 
			}
			?>
		</select>
	</div>

		<input type="hidden" name="idNoticia" value="<?php echo $idNoticia; ?>">

	<button type="sumbit" name="enviar" class="btn btn-dfault">
		Enviar
	</button>
</form>

<?php 
}
?>