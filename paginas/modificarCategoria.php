<h3>
	Modificar Categoría
	-
	<small>
		<a href="index.php?p=categorias.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if (isset($_POST['enviar'])){
	//Modifico la categoria	 
	//Recojo los datos que quiero modificar
	$nombre=$_POST['nombre'];
	$id=$_POST['idCategoria'];

	//Establezco consulta
	$sql="UPDATE categorias SET nombreCategoria='$nombre' WHERE idCategoria=$id";
	
	
	//Ejecuto la consulta y/o Muestro el mensaje
	if($consulta=$conexion->query($sql)){
		header('Refresh: 2; url=index.php?p=categorias.php');
		?>
		<div class="alert alert-success">
			<strong>TODO OK!!</strong>
			Modificación realizada con éxito
			<img src="imagenes/cargando.gif" width="50">
		</div>
		<?php	
	}else{
		?>
		<div class="alert alert-danger">
			<strong>ERROR!!</strong>
			No se ha podido realizar
		</div>
		<?php
	}
}else{
	//Muestro el formulario de modificación
	//Necesito el id de la categoria a modificar para poder actuar sobre ella
	$idCategoria=$_GET['idCategoria'];
	$sql="SELECT * FROM categorias WHERE idCategoria=$idCategoria";
	$consulta=$conexion->query($sql);
	$registro=$consulta->fetch_array();
?>
<form action="index.php?p=modificarCategoria.php" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="nombre">Nombre de la categoría:</label>
		<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $registro['nombreCategoria']; ?>">
	</div> 

	<input type="hidden" name="idCategoria" value="<?php echo $idCategoria; ?>">

	<button type="sumbit" name="enviar" class="btn btn-dfault">
		Enviar
	</button>
</form>

<?php 
}
?>