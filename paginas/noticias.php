

<h3>
Estás en el listado de noticias
	-
	<?php
	if($_SESSION['conectado']){ 
	?>
	<small>
		<a href="index.php?p=insertar.php">Insertar noticia</a>
	</small>
	<?php
	} 
	?>
</h3>


<?php


//Establecer la consulta a la base de datos en SQL
if(isset($_GET['idCategoria'])){
	$idCategoria=$_GET['idCategoria'];
	$sql="SELECT * FROM noticias INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria WHERE noticias.idCategoria=$idCategoria";
}else{
	$sql="SELECT * FROM noticias INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria";
}



//Ejecutar la pregunta o consulta

$consulta=$conexion->query($sql);

//Procesamos los resultados de la pregunta

while($registro=$consulta->fetch_array()){
	?>
	<article>
		<header>
			<h4>
				<a href="index.php?p=detalle.php&idNoticia=<?php echo $registro['idNoticia'];?>">
					<strong><?php echo $registro['tituloNoticia']; ?></strong>
				</a>
				<?php 
					if($_SESSION['conectado']){
				?>
				-
				<a href="index.php?p=borrar.php&idNoticia=<?php echo $registro['idNoticia'];?>">
					<span class="glyphicon glyphicon-trash" style="color: red;"></span>
				</a>
				-
				<a href="index.php?p=modificar.php&idNoticia=<?php echo $registro['idNoticia'];?>">
					<span class="glyphicon glyphicon-pencil" style="color: green;"></span>
				</a>
				<?php } ?>
			</h4>
			<small>
				<?php echo $registro['nombreCategoria']; ?>
			</small>
		</header>
		<section>
			<img src="imagenes/<?php echo $registro['imagenNoticia']; ?>" class="img-responsive img-rounded" style="float:left; margin:10px; width: 200px;">
			<?php echo substr($registro['textoNoticia'],0,100); ?>
			<a href="index.php?p=detalle.php&idNoticia=<?php echo $registro['idNoticia'];?>">Leer más ...</a>
		</section>
		<footer class="text-right" style="clear: both;">
			<?php echo $registro['fechaNoticia']; ?>
		</footer>
	</article>



<?php
}

?>