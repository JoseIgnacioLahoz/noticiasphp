-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-05-2018 a las 13:06:35
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `noticiasphp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCategoria` int(11) NOT NULL,
  `nombreCategoria` varchar(200) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCategoria`, `nombreCategoria`) VALUES
(1, 'Actualidad'),
(2, 'Deporte'),
(3, 'Informática'),
(4, 'Juegos'),
(5, 'Televisión'),
(6, 'Economía'),
(7, 'Cine'),
(8, 'Opinión'),
(9, 'Local'),
(10, 'Música'),
(11, 'Internacional'),
(12, 'Publicidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `idNoticia` int(11) NOT NULL,
  `tituloNoticia` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `textoNoticia` longtext COLLATE utf8_spanish_ci NOT NULL,
  `fechaNoticia` datetime NOT NULL,
  `imagenNoticia` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `idCategoria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idNoticia`, `tituloNoticia`, `textoNoticia`, `fechaNoticia`, `imagenNoticia`, `idCategoria`) VALUES
(2, 'El niño que se quedó atrapado en su pupitre', 'El trabajo de los Bomberos es duro, y sus profesionales se tienen que enfrentar a realidades que incluyen muertes y un amplio historial de tragedias. Sin embargo, en su hoja de servicios también hay lugar para intervenciones menos dramáticas y más curiosas que, en algunos casos, seguro que dieron para más de un chascarrillo entre los profesionales en los pasillos del parque de Bomberos.\r\n\r\nSegún la memoria del Cuerpo de Bomberos del año 2017, el día 3 de enero de ese año, por ejemplo, una unidad acudió a la calle Francisco de Vitoria para liberar a una persona... que se había quedado enganchada a un radiador con su pulsera. Dos días antes, en Año Nuevo, recuperaron un móvil del interior de una alcantarilla en la calle Contamina. El día 7 de ese mismo mes, usaron la autoescala en la calle Santa Orosia para recuperar una bolsa con dinero que acabó en un alféizar al ser lanzada desde un piso, no se sabe por qué ni con qué objetivo.', '2018-04-09 00:00:00', 'noticia2.jpg', 1),
(3, 'Detienen a Fluvi por apoyar las inundaciones en Aragón ', 'La que fuera mascota de la  Expo Zaragoza 2008, ha sido detenida esta mañana por enaltecer las inundaciones que Aragón está sufriendo estos días.\r\nEn torno a las 9:34 a.m., la Policía Nacional ha irrumpido en la vivienda de éste y se lo han llevado a comisaria. El principal motivo de su detención han sido varios varios tuits en los que se alegraba de lo grave situación que está pasando la comunidad autónoma aragonesa. “Soy una gota de agua y tengo derecho a defender a los míos”, gritaba mientras se lo llevaban detenido.\r\n\r\nFluvi y su odio hacia el lugar que le vio nacer\r\nNadie entiende que Fluvi, una mascota pacífica y cien por cien aragonesa, sienta tanto odio y repulsión por Aragón. Ni su familia, amigos y abogados entienden lo que le ha llevado a la mascota de la Expo para que se alegre tanto de lo que está sucediendo en su tierra.\r\n\r\n“Somos los primeros sorprendidos”, comentaba el padre de Fluvi. “Sí que es cierto que hace unos días se empezó a juntar mucho con gente de Murcia que le empezó a contar historias sobre el trasvase, pero no le dimos mucha importancia”, añadía.\r\n\r\nLos abogados de Fluvi, preocupados ante su falta de arrepentimiento\r\nLa defensa del acusado anda de los nervios debido a que Fluvi se niega a retractarse por sus polémicas declaraciones, y es que de no hacerlo, éste podría acabar en la cárcel. “Nuestro cliente se queja que sus palabras causen tanto impacto cuando él nunca se ha quejado del nombre que le han puesto ni de su diseño”, decía uno de los abogados.\r\n\r\n“Lo que he dicho lo pienso de verdad y me niego a que me quiera coartar de esta forma”, declaraba Fluvi. “Nadie sabe por lo que me hicieron pasar durante la Expo. Cada uno tiene lo que se merece”, añadía.\r\nSegún ha podido saber HERALDO, la víctima no portaba documentación, por lo que el Equipo de Atestado de la Guardia Civil trata ahora de identificarlo. Aunque junto al lugar del accidente se encuentran varias fábricas -entre otras, la planta de Jacob Delafon, no parece que fuera un empleado que entrara o saliera de trabajar. De hecho, todo apunta a un indigente que caminaba por el arcén.\r\n\r\nLos equipos de emergencia solo han ocupado el carril derecho de la calzada, lo que ha permitido a los agentes de la Benemérita dar paso alternativo por el izquierdo. En cualquier caso y dada la hora a la que se ha producido el atropello, se han formado algunas retenciones. ', '2018-04-09 00:00:00', 'noticia7.jpg', 9),
(4, 'El Ayuntamiento de Zaragoza utilizará los cojones de los jugadores del Real Zaragoza como bolardos', 'El alcalde de Zaragoza ha tomado la decisión de reemplazar los bolardos que hay por la ciudad por los testículos de los jugadores del equipo blanquillo.\r\nLa brillante actuación del Real Zaragoza en esta segunda vuelta de la Liga 123 ha convertido al equipo en el mejor de su categoría en este 2018, siendo un serio candidato para el ascenso directo. Su última victoria frente a la Sociedad Deportiva Huesca fue clave para que Pedro Santisteve viera en las zonas íntimas de los jugadores una herramienta para proteger la ciudad.\r\n\r\nLos cojones del equipo, nuevo mobiliario urbano\r\nLa épica remontada del Real Zaragoza en la clasificación ha sido el motivo que ha convencido al pleno municipal de Zaragoza para proponer que los nuevos bolardos y maceteros que ocupan la ciudad sean los huevos de estos jugadores que se están dejando la piel en cada partido.\r\n\r\n“Los bolardos actuales no tienen nada que ver con la fuerza, dureza y resistencia de los testículos de Borja Iglésias, Crístian Álvarez, Alberto Zapater o Íñigo Eguaras”, comentaba uno de los concejales.\r\n\r\nMás duros que el diamante\r\nLos bolardos y pilonas que se dejan ver por las calles de Zaragoza están hechos de piedra y conglomerado, un material duro y consistente, pero bastante menos fuerte y resiste que el material del que están hechos los cojones de los jugadores y cuerpo técnico del Real Zaragoza.\r\n\r\n“Es una cuestión de seguridad”, declaraba el alcalde. “Si queremos fiarnos y estar tranquilos necesitamos trabajar con los mejores materiales, y ahora no hay elemento más fiable que las gónadas de nuestros jugadores”, explicaba.', '0000-00-00 00:00:00', 'noticia4.jpg', 2),
(7, 'Zaragoza y Huesca decidirán tras el derbi quién se queda con Teruel', 'El derbi aragonés, más allá del resultado, tendrá un aliciente añadido una vez se dispute el próximo sábado: quién se quedará con Teruel.\r\nDurante estos últimos años se ha hablado mucho sobre Huesca y Teruel, dejando de lado a la tercera provincia aragonesa: Teruel, por eso, después de finalizar el encuentro, el equipo que gane se hará con los derechos de la ciudad turolense. Aunque aún están por decidir las condiciones, las tres ciudades estarían de acuerdo con este pacto.\r\n\r\nEl derbi aragonés, mucho más que un partido\r\nEl subidón de la S.D.Huesca en estos dos años y la irregularidad del equipo maño han hecho que a nivel futbolístico, sean los equipos más mediáticos de la temporada, lo que ha hecho olvidar; una vez más, a Teruel. Y es que Huesca se le ha comido el terreno, lo que ha propiciado a la hermandad de los tres ayuntamientos para que gane quien gane, Teruel también lo haga y forme parte de Zaragoza o Huesca.\r\n\r\n“Nosotros preferimos que gane el Real Zaragoza porque es la capital, pero el Huesca va directo a primera, así que nos da igual el equipo que salga vencedor”, explicaba el alcalde.\r\n\r\nTeruel ya está haciendo planes de futuro\r\nActualmente, Teruel es la capital de provincia menos poblada de España, de ahí que éste tan interesada en esta iniciativa ya que les ayudaría para aumentar en población e incluso podrían sumar en demanda turística y crecimiento del mercado, y con lo que sobre: construir un Primark y que el Club Deportivo Teruel esté en Primera División en los próximos cuatro años. Estas son las ideas que el Ayuntamiento de Teruel tiene mente cuando sea apadrinado por Zaragoza o Huesca.\r\n\r\n“La que más nos interesa es la de subir con el C.D. Teruel a Primera, el resto es un poco secundario, porque si subimos seremos más conocidos y querrá venir a vivir más gente. Se cae de cajón”, comentaba uno de los concejales.', '0000-00-00 00:00:00', 'noticia5.jpg', 1),
(8, 'Aragón podría convertirse en la nueva Atlántida si el Ebro sigue creciendo', 'La comunidad aragonesa podría sustituir a la Atlántida como el territorio poblado hundido más grande del mundo.\r\nLa continua crecida del Ebro y de sus afluentes ha hecho crecer la alarma en la Comunidad Autónoma, considerando un hecho la posibilidad de acabar sumergidos. De seguir lloviendo así, Aragón se sitúa como un nuevo ejemplo de comunidad vencida por el agua, y aunque la gravedad del asunto sea alta el ayuntamiento está tomando las medidas necesarias para que no cunda el pánico.\r\n\r\nLa Atlántida española\r\nLa Confederación Hidrográfica del Ebro ya ha alertado de que la situación continuará así durante los próximos días, lo que hace pensar que Aragón acabará sumergida en las próximas horas. El Gobierno de Aragón, después de conocer que las altas probabilidades de que esto ocurra, ya ha lanzado varios cursos de submarinismo y de apnea para poder sobrevivir a esta crecida del Río Ebro.\r\n\r\n“Estos cursos serán gratuitos”, avisan desde la DGA. “Hay que ver esta crecida como una oportunidad para crecer como comunidad, y es que nos convertiremos en referencia mundial en natación. Nadie podrá ganarnos en las próximas Olimpiadas”, comentaban.\r\n\r\nAragoneses y aragonesas ya tienen todo preparado\r\nNuestra Comunidad Autónoma está más que acostumbrada a las crecidas del río Ebro, por eso sabían que cualquier día podía ocurrir algo como esto. Bañadores, bikinis, bombonas de oxígeno, gafas de bucear, manguitos; de esto se componen estos kits para sobrellevar la crecida.\r\n\r\n“La verdad es que no estamos muy preocupados”, comentaba un vecino de Alcalá de Ebro. “Le hemos puesto los manguitos a la abuela, el flotador al pequeño y nosotros aprovecharemos e iremos a ver los Siluros”, añadía emocionado.', '0000-00-00 00:00:00', 'noticia6.jpg', 8),
(9, 'Se entera a los 17 años que Miguel Servet fue un científico aragonés y no un hospital', 'Ismael Bermejo ha descubierto que Miguel Servet fue un reconocido científico y no un hospital.\r\nLa noticia la ha recibido tras la corrección de un examen de Historia de España en el que era preguntado sobre quién fue Miguel Servet, a lo que respondió que se trataba de uno de los hospital más importantes de la capital aragonesa. La contestación de éste dejo a la profesora sin palabras, obligando a tratar este caso con los padres del alumno personalmente.\r\n\r\nIsmael, una persona especial\r\nLa respuesta a este punto del examen ha calado hondo en el instituto del muchacho, tanto que la profesora, el director del centro, los padres y el alumno se han juntado para hablar sobre lo sucedido y ver dónde reside el problema a semejante desconocimiento. El chaval, que lleva desde los seis años en este colegio, ya destacaba cosas como estas.\r\n\r\n“Profesores que le han dado clases ya me comentaban cosas sobre Ismael”, comentaba la tutora. “La tabla del 5 fue la que más le costó, dice ‘dijistes’, escucha música sin auriculares en público y creía en los Reyes Magos hasta el año pasado”, decía.\r\n\r\nSus padres ya avisaron al centro de sus capacidades\r\nJesús Manuel y Cristina, los padres del chico, ya comentaron en su día que Ismael tiene problemas de concentración y vive en un mundo diferente al del resto, por eso ya no les pilla por sorpresa este tipo de cosas.\r\n\r\n“Claro que nos preocupa, pero qué le vamos a hacer. El niño lo intenta, pero no da para más”, contaba el padre. “Esto no es nuevo para nosotros, él también pensaba que Palafox eran solo unos cines”, añadía.', '0000-00-00 00:00:00', 'noticia8.jpg', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` text COLLATE utf8_spanish_ci NOT NULL,
  `login` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `session` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `apellidos`, `login`, `correo`, `password`, `session`) VALUES
(1, 'Jose Ignacio', 'Lahoz Felez', 'admin', 'admin@gmail.com', 'ee10c315eba2c75b403ea99136f5b48d', '');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCategoria`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNoticia`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNoticia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
